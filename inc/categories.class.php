<?php

class Category{
    private $category_name;
    private $category_id;
    private $products;
    
    public function __construct($category_name = null, $category_id = null, $products = null){
        if (null !== $category_name){
            $this->category_name = $category_name;
        }
        
        if (null !== $category_id){
            $this->category_id = $category_id;
        }
        
        if (null !== $products){
            $this->products = $products;
        }
    }
    
    public function setCategoryName($categoryName){
        $this->category_name = $categoryName;
    }
    
    public function getCategoryName(){
        return $this->category_name;
    }
    
    public function setCategoryId($categoryId){
        $this->category_id = $categoryId;
    }
    
    public function getCategoryId(){
        return $this->category_id;
    }
    
    public function setProducts($products){
        $this->products = $products;
    }
    
    public function getProducts(){
        return $this->products;
    }
    
    public function convertDbObject($dbRow){
        $this->category_name = $dbRow['category_name'];
        $this->category_id = $dbRow['category_id'];
        $this->products = $dbRow['products'];
    }
    
    public function toStdObject (){
        $object = new stdClass();
        $object->category_id = $this->category_id;
        $object->category_name = $this->category_name;
        $object->products = $this->products;
        
        return $object;
    }
}

?>
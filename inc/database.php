<?php

require 'categories.class.php';
require 'products.class.php';

class Database {

    private $con;

    public function __construct($host, $username, $password, $database) {
        $this->con = new PDO("mysql:host=" . $host . ";dbname=" . $database, $username, $password);
    }

    public function getCategories() {

        $sql = "
            SELECT
                c.category_name,
                c.category_id,
                count(p.product_id)
            AS 
                products
            FROM 
                category c
            LEFT JOIN 
                product p 
            ON
                p.category_id = c.category_id
            GROUP BY
                p.category_id 
            ORDER By 
                c.category_id
            ;
        ";

        $statement = $this->con->prepare($sql);
        $statement->execute();

        while ($row = $statement->fetch()) {
            $myCategory = new Category();
            $myCategory->convertDbObject($row); 
            $dataSet[] = $myCategory->toStdObject();
        }
        if (!empty($dataSet)) {
            return $dataSet;
        } else {
            return null;
        }
    }

    public function getProducts() {
        
        $sql = "
            SELECT
                * 
            FROM 
                product 
            WHERE 
                category_id = 1 
            ORDER BY 
                product_id
            ;
        ";
        
        $statement = $this->con->prepare($sql);
        $statement->execute();
            while ($row = $statement->fetch()) {
            $myProduct = new Product();
            $myProduct->convertDbObject($row); 
            $dataSet[] = $myProduct->toStdObject();
        }
            if (!empty($dataSet))
                return $dataSet;
            else
                return null;
    }

}

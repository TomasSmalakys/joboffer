<?php

class Product{
    private $product_id;
    private $category_id;
    private $product_name;
    private $price;
    private $pvm;

    
    public function __construct($product_id = null, $category_id = null, $product_name = null, $price = null, $pvm = null){
        if (null !== $product_id){
            $this->product_id = $product_id;
        }
        
        if (null !== $category_id){
            $this->category_id = $category_id;
        }
        if(null !== $product_name){
        $this->product_name = $product_name;
        }
        
        if (null !== $price) {
            $this->price = $price;
        }
        
        if (null !== $pvm){
            $this->pvm = $this->price - round($this->price*100/121, 2);
        }
    }
    
    public function getProductId(){
        return $this->product_id;
    }
    
    public function getCategoryId(){
        return $this->category_id;
    }
    
    public function getProductName(){
        return $this->product_name;
    }
    
    public function getPrice(){
        return $this->price;
    }
    
    public function convertDbObject($dbRow){
        $this->category_id = $dbRow['category_id'];
        $this->product_id = $dbRow['product_id'];
        $this->product_name = $dbRow['product_name'];
        $this->price = $dbRow['price'];
    }
    
    public function toStdObject (){
        $object = new stdClass();
        $object->product_id = $this->product_id;
        $object->category_id = $this->category_id;
        $object->product_name = $this->product_name;
        $object->price = $this->price;
        $object->pvm = $this->pvm = $this->price - round($this->price*100/121, 2);
        
        return $object;
    }
}

?>
<?php
require_once('inc/database.php');
require_once('libs/Smarty.class.php');
$smarty = new Smarty();
$smarty->setTemplateDir($_SERVER["DOCUMENT_ROOT"].'/joboffer/templates');
$xml = file_get_contents('php://input');
$xml = simplexml_load_string($xml);
$db = new Database("localhost","root","","job");

    if($xml->action == 'getCategories'){
    
        $dataSet = $db->getCategories();

        $smarty->assign('categories', $dataSet);
        $smarty->display('categories.tpl');
}

    elseif ($xml->action == 'getProducts') {

        $dataSet = $db->getProducts();

        $smarty->assign('products', $dataSet);
        $smarty->display('products.tpl');
}
?>